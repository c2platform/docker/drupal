#!/bin/bash
set -e

/var/www/html/vendor/bin/drush updatedb --no-cache-clear -y || echo "ERROR: Failed updb."
/var/www/html/vendor/bin/drush cr -y || echo "ERROR: Failed cr."
/var/www/html/vendor/bin/drush cim -y || echo "ERROR: Failed cim."
/var/www/html/vendor/bin/drush cr -y || echo "ERROR: Failed cr."
/var/www/html/vendor/bin/drush deploy:hook -y || echo "ERROR: Failed deploy hook."

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php "$@"
fi

exec "$@"